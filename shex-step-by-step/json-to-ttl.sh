#! /bin/bash



if [ ! -d "venv" ]; then
  echo "venv directory does not exist"

  echo "Installing virtualenv"
  pip3 install --user virtualenv

  echo "Creating Virtual Environment"
  virtualenv venv -p python3

  echo "Activating Virtual Environment"
  . venv/bin/activate

  echo "Installing fhirtordf"
  pip3 install fhirtordf

  echo "Deactivating Virtual Environment"
  deactivate

fi

echo "Activating Virtual Environment"
. venv/bin/activate

echo "Converting JSON Files into RDF"
#fhirtordf -i resources/p.json resources/c1.json resources/c2.json resources/pr1.json resources/pr2.json -mv vocabulary/fhir.ttl -u http://dss.cora/ -no -o ttls/all.ttl
fhirtordf -id resources -sd xxx -mv vocabulary/fhir.ttl -u http://dss.cora/ -no -o ttls/all.ttl

deactivate

echo "Done"
