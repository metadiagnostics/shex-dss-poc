/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saperi.shex.sample.poc.dss;

import com.saperi.shex.sample.poc.driver.sample.DSSFhirClient;
import com.saperi.shex.sample.poc.driver.sample.SampleConditionDriver;
import com.saperi.shex.sample.poc.driver.sample.SampleMedicationAdministrationDriver;
import com.saperi.shex.sample.poc.driver.sample.SamplePatientDriver;
import com.saperi.shex.sample.poc.fhir.MockFhirService;
import java.util.Arrays;
import java.util.Date;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Condition;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.MedicationAdministration;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Reference;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class QueryProcessorTest {
    
    private DSSFhirClient fhir;
    
    @Before
    public void doBefore(){
        fhir = new MockFhirService();
    }

    @Test
    public void doPatientTest() {
        
        this.createPatient("1");
        this.createPatient("2");

        String shEx = ""
                + "PREFIX :         <http://example.org/>\n"
                + "PREFIX dss:      <http://saperi.io/dss/>\n"
                + "PREFIX xsd:      <http://www.w3.org/2001/XMLSchema#>\n"
                
                + ":PatientShape {\n"
                + "  a [dss:patient] ;\n"
                + "  dss:patient.birth-date @:Date ;\n"
                + "}\n"
        
                + ":Date xsd:dateTime OR xsd:date OR xsd:year\n";

        String shapeMap = "{FOCUS a <http://saperi.io/dss/patient>}@:PatientShape";

        QueryProcessor qp = new QueryProcessor(Arrays.asList(
                new SamplePatientDriver(fhir),
                new SampleConditionDriver(fhir)
        ));

        qp.query(shEx, shapeMap);

    }
    
    @Test
    public void doPatientDiabetesTest() {
        
        this.createPatient("1");
        this.createCondition("1", "73211009", "Diabetes");
        
        this.createPatient("2");
        this.createCondition("2", "2707005", "Necrotizing Enterocolitis");

        String shEx = ""
                + "PREFIX :         <http://example.org/>\n"
                + "PREFIX dss:      <http://saperi.io/dss/>\n"
                + "PREFIX xsd:      <http://www.w3.org/2001/XMLSchema#>\n"
                
                + ":PatientShape {\n"
                + "  a [dss:patient] ;\n"
                + "  dss:patient.id [\"1\" \"2\"] ;\n"
                + "  dss:patient.birth-date @:Date ;\n"
                + "  dss:patient.diabetes [true] ;\n"
                + "}\n"
        
                + ":Date xsd:dateTime OR xsd:date OR xsd:year\n";

        String shapeMap = "{FOCUS a <http://saperi.io/dss/patient>}@:PatientShape";

        QueryProcessor qp = new QueryProcessor(Arrays.asList(
                new SamplePatientDriver(fhir),
                new SampleConditionDriver(fhir)
        ));

        qp.query(shEx, shapeMap);

    }
    
    @Test
    public void doPatientOnInsulinTest() {
        
        this.createPatient("1");
        this.createMedicationAdministration("1", "67866001", "Insulin (substance)");
        
        this.createPatient("2");
        this.createMedicationAdministration("2", "372711004", "Sulfonylurea (substance)");

        String shEx = ""
                + "PREFIX :         <http://example.org/>\n"
                + "PREFIX dss:      <http://saperi.io/dss/>\n"
                + "PREFIX xsd:      <http://www.w3.org/2001/XMLSchema#>\n"
                
                + ":PatientShape {\n"
                + "  a [dss:patient] ;\n"
                + "  dss:patient.id [\"1\" \"2\"] ;\n"
                + "  dss:patient.birth-date @:Date ;\n"
                + "  dss:patient.on-insulin [true] ;\n"
                + "}\n"
        
                + ":Date xsd:dateTime OR xsd:date OR xsd:year\n";

        String shapeMap = "{FOCUS a <http://saperi.io/dss/patient>}@:PatientShape";

        QueryProcessor qp = new QueryProcessor(Arrays.asList(
                new SamplePatientDriver(fhir),
                new SampleConditionDriver(fhir),
                new SampleMedicationAdministrationDriver(fhir)
        ));

        qp.query(shEx, shapeMap);
    }
    
    @Test
    public void doPatientOnInsulinAndSulfonylureaAndDiabetesTest() {
            
        this.createPatient("1");
        this.createMedicationAdministration("1", "67866001", "Insulin (substance)");
        
        this.createPatient("2");
        this.createMedicationAdministration("2", "372711004", "Sulfonylurea (substance)");
        
        this.createPatient("3");
        this.createMedicationAdministration("3", "67866001", "Insulin (substance)");
        this.createMedicationAdministration("3", "372711004", "Sulfonylurea (substance)");
        
        this.createPatient("4");
        this.createMedicationAdministration("4", "67866001", "Insulin (substance)");
        this.createMedicationAdministration("4", "372711004", "Sulfonylurea (substance)");
        this.createCondition("4", "73211009", "Diabetes");
        

        String shEx = ""
                + "PREFIX :         <http://example.org/>\n"
                + "PREFIX dss:      <http://saperi.io/dss/>\n"
                + "PREFIX xsd:      <http://www.w3.org/2001/XMLSchema#>\n"
                
                + ":PatientShape {\n"
                + "  a [dss:patient] ;\n"
                + "  dss:patient.id [\"1\" \"2\" \"3\" \"4\"] ;\n"
                + "  dss:patient.birth-date @:Date ;\n"
                + "  dss:patient.on-insulin [true] ;\n"
                + "  dss:patient.on-sulfonylurea [true] ;\n"
                + "  dss:patient.diabetes [true] ;\n"
                + "}\n"
        
                + ":Date xsd:dateTime OR xsd:date OR xsd:year\n";

        String shapeMap = "{FOCUS a <http://saperi.io/dss/patient>}@:PatientShape";

        QueryProcessor qp = new QueryProcessor(Arrays.asList(
                new SamplePatientDriver(fhir),
                new SampleConditionDriver(fhir),
                new SampleMedicationAdministrationDriver(fhir)
        ));

        qp.query(shEx, shapeMap);
    }
    
    private Patient createPatient(String id){
        Patient patient = new Patient();
        patient.setId(id);
        patient.setBirthDate(new Date());
        patient = fhir.createOrUpdateResource(patient);
        
        return patient;
    }
    
    private MedicationAdministration createMedicationAdministration(String patientId, String code, String display){
        MedicationAdministration medicationAdministration = new MedicationAdministration();
        medicationAdministration.setMedication(this.createCodeableConcept("http://snomed.info/sct", code, display));
        medicationAdministration.setSubject(this.createReference(patientId));
        medicationAdministration = fhir.createOrUpdateResource(medicationAdministration);
        
        return medicationAdministration;
    }
    
    private Condition createCondition(String patientId, String code, String display){
        Condition condition = new Condition();
        condition.setCode(this.createCodeableConcept("http://snomed.info/sct", code, display));
        condition.setSubject(this.createReference(patientId));
        condition = fhir.createOrUpdateResource(condition);
        
        return condition;
    }
    
    private CodeableConcept createCodeableConcept(String system, String code, String display){
        return new CodeableConcept().addCoding(new Coding(system, code, display));
    }
    
    private Reference createReference(String id){
        return new Reference(new IdType("", id));
    }

}
