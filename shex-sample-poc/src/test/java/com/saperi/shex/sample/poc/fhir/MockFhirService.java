/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saperi.shex.sample.poc.fhir;

import ca.uhn.fhir.rest.client.IGenericClient;
import com.saperi.shex.sample.poc.driver.sample.DSSFhirClient;
import com.saperi.shex.sample.poc.driver.sample.SampleUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import static java.util.stream.Collectors.toList;
import org.hl7.fhir.dstu3.model.Base;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Condition;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.MedicationAdministration;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;

/**
 *
 * @author esteban
 */
public class MockFhirService extends DSSFhirClient {

    public MockFhirService() {
        super((IGenericClient) null);
    }

    private Map<String, IBaseResource> resources = new HashMap<>();

    @Override
    public <T extends IBaseResource> T createOrUpdateResource(T resource) {
        String id = resource.getIdElement() != null && resource.getIdElement().getIdPart() != null ? resource.getIdElement().getIdPart()
                : UUID.randomUUID().toString();
        IBaseResource r = resources.get(id);
        if (r == null) {
            resource.setId(id);
        }
        this.resources.put(id, resource);

        return resource;
    }

    public Map<String, IBaseResource> getResources() {
        return resources;
    }

    public <T extends IBaseResource> List<T> searchResourceByIdentifier(String codeSystem, String code, Class<T> clazz) {

        List<T> results = new ArrayList<>();
        if (!Resource.class.isAssignableFrom(clazz)) {
            return results;
        }

        for (IBaseResource r : resources.values()) {
            if (clazz.isAssignableFrom(r.getClass())) {
                try {
                    Base[] identifiers = ((Resource) r).getProperty("identifier".hashCode(), "identifier", true);
                    if (identifiers == null) {
                        continue;
                    }

                    String c = Arrays.stream(identifiers).filter(id -> id instanceof Identifier).map(id -> (Identifier) id)
                            .filter(id -> codeSystem.equals(id.getSystem())).findFirst()
                            .orElse(new Identifier().setSystem(codeSystem).setValue(null)).getValue();

                    if (code.equals(c)) {
                        results.add((T) r);
                    }
                } catch (FHIRException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        }

        return results;
    }

    @Override
    public <T extends IBaseResource> T searchResourceById(String resourceId, Class<T> clazz) {
        IBaseResource resource = resources.get(resourceId);

        if (resource == null) {
            return null;
        }

        if (!clazz.isAssignableFrom(resource.getClass())) {
            throw new IllegalArgumentException("The resource with id '" + resourceId + "' is not of type '" + clazz.getName() + "' but of type '"
                    + resource.getClass().getName() + "'");
        }

        return (T) resource;
    }

    public <T extends IBaseResource> T searchResourceById(IIdType idType, Class<T> clazz) {
        String id = idType.getIdPart();
        return this.searchResourceById(id, clazz);
    }

    @Override
    public <T extends IBaseResource> List<T> searchResourcesByType(Class<T> clazz) {
        return resources.values().stream().filter(r -> clazz.isAssignableFrom(r.getClass())).map(r -> (T) r).collect(toList());
    }

    @Override
    public <T extends IBaseResource> List<T> searchResourcesForPatient(String patientId, Class<T> clazz) {

        List<T> results = new ArrayList<>();

        for (IBaseResource r : resources.values()) {
            if (!clazz.isAssignableFrom(r.getClass())) {
                continue;
            }

            String resourcePatientId = null;
            if (r instanceof Condition) {
                resourcePatientId = ((Condition) r).getSubject().getReferenceElement().getIdPart();
            } else if (r instanceof MedicationAdministration) {
                resourcePatientId = ((MedicationAdministration) r).getSubject().getReferenceElement().getIdPart();
            } else if (r instanceof Patient) {
                //Skip
            } else {
                //we fail on purpose to make sure we haven't forget to add
                //spport for a specific class
                throw new UnsupportedOperationException("Class " + clazz + " is not supported by this mock implementation yet.");
            }

            if (resourcePatientId != null && resourcePatientId.equals(patientId)) {
                results.add((T) r);
            }
        }

        return results;
    }

    @Override
    public <T extends IBaseResource> List<T> searchResourcesForPatientAndCode(String patientId, Coding code, Class<T> clazz) {
        List<T> resources = this.searchResourcesForPatient(patientId, clazz);

        List<T> results = new ArrayList<>();

        for (IBaseResource r : resources) {
            CodeableConcept cc = this.extractCodeableConcept(r);

            if (cc != null && SampleUtils.hasCoding(cc, code)) {
                results.add((T) r);
            }
        }

        return results;
    }

    @Override
    public <T extends IBaseResource> List<T> searchResourcesByCoding(String system, String code, Class<T> clazz) {
        List<T> resources = this.searchResourcesByType(clazz);

        List<T> results = new ArrayList<>();

        for (IBaseResource r : resources) {
            CodeableConcept cc = this.extractCodeableConcept(r);

            if (cc != null && SampleUtils.hasCoding(cc, system, code)) {
                results.add((T) r);
            }
        }

        return results;
    }

    private CodeableConcept extractCodeableConcept(IBaseResource r) {
        try {
            if (r instanceof Condition) {
                return ((Condition) r).getCode();
            } else if (r instanceof MedicationAdministration) {
                return ((MedicationAdministration) r).getMedicationCodeableConcept();
            } else if (r instanceof Patient) {
                return null;
            } else {
                //we fail on purpose to make sure we haven't forget to add
                //spport for a specific class
                throw new UnsupportedOperationException("Class " + r.getClass() + " is not supported by this mock implementation yet.");
            }
        } catch (FHIRException ex) {
            throw new IllegalStateException(String.format("Exception extracting CodeableConcept from %s/%s.", r.getClass().getName(), r.getIdElement().getIdPart()), ex);
        }
    }

}
