/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.saperi.shex.sample.poc;

import es.weso.rdf.nodes.RDFNode;
import es.weso.shapeMaps.Info;
import es.weso.shapeMaps.ShapeMapLabel;
import fr.inria.lille.shexjava.GlobalFactory;
import fr.inria.lille.shexjava.schema.Label;
import fr.inria.lille.shexjava.schema.ShexSchema;
import fr.inria.lille.shexjava.schema.abstrsynt.ShapeExpr;
import fr.inria.lille.shexjava.schema.parsing.Parser;
import fr.inria.lille.shexjava.schema.parsing.ShExCParser;
import fr.inria.lille.shexjava.validation.RecursiveValidation;
import fr.inria.lille.shexjava.validation.ValidationAlgorithm;
import java.io.InputStream;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.rdf4j.RDF4J;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class SimpleTest {

    private InputStream schemaIs;
    private InputStream dataIs;

    @Before
    public void doBefore() {
        schemaIs = SimpleTest.class.getResourceAsStream("/ConditionProcedure/ConditionProcedure.shex");
        dataIs = SimpleTest.class.getResourceAsStream("/ConditionProcedure/ConditionProcedure.ttl");
    }

    @Test
    public void doTest() throws Exception {

        RDF4J factory = new RDF4J();
        GlobalFactory.RDFFactory = factory;

        //Schema
        Parser parser = new ShExCParser();
        Map<Label, ShapeExpr> rules = parser.getRules(GlobalFactory.RDFFactory, schemaIs);
        ShapeExpr start = parser.getStart();
        ShexSchema schema = new ShexSchema(GlobalFactory.RDFFactory, rules, start);

        //Data
        Model data = Rio.parse(dataIs, "http://saperi.com/sample", RDFFormat.TURTLE);
        Graph dataGraph = factory.asGraph(data);

        //Shape
        //TODO: How to use {FOCUS} instead of having to point to a specific node?
        IRI focusNode = factory.createIRI("http://dss.cora/Condition/2efcdad3-1e87-4acc-a2a0-2770aad8bc6c");
        Label shapeLabel = new Label(factory.createIRI("http://example.org/ConditionShape"));

        //Validation
        ValidationAlgorithm validation = new RecursiveValidation(schema, dataGraph);
        validation.validate(focusNode, shapeLabel);

        //check the result
        boolean result = validation.getTyping().isConformant(focusNode, shapeLabel);
        System.out.println("Does " + focusNode + " has shape " + shapeLabel + "? " + result);

    }

    @Test
    public void doTest2() throws Exception {

        String schema = IOUtils.toString(schemaIs, "UTF-8");
        String data = IOUtils.toString(dataIs, "UTF-8");

        String shapeMap = "{FOCUS a fhir:Condition}@:MotionSicknessShape";

        Validate validator = new Validate();
        Result result = validator.validate(data, "Turtle", schema, "ShExC", shapeMap, "Compact").toOption().get();

//        System.out.println("Result (JSON): " + result.getResultShapeMap().serialize("JSON"));
//        System.out.println("Result (COMPACT): " + result.getResultShapeMap().serialize("COMPACT"));

        scala.collection.immutable.Map<RDFNode, scala.collection.immutable.Map<ShapeMapLabel, Info>> map = result.getResultShapeMap().resultMap();

        map.foreach((t) -> {
            System.out.println("Node: " + t._1.toString());
            t._2.foreach(m -> {
                System.out.print("\tShape: " + m._1.toString());
                System.out.println(" => " + m._2.status());
                return null;
            });

            return null; //To change body of generated lambdas, choose Tools | Templates.
        });

    }
}
