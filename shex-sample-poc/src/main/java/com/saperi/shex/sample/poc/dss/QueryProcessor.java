/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.saperi.shex.sample.poc.dss;

import com.saperi.shex.sample.poc.Result;
import com.saperi.shex.sample.poc.Validate;
import com.saperi.shex.sample.poc.driver.Driver;
import es.weso.rdf.nodes.RDFNode;
import es.weso.shapeMaps.Association;
import es.weso.shapeMaps.Conformant$;
import es.weso.shapeMaps.Info;
import es.weso.shapeMaps.ShapeMapLabel;
import es.weso.shex.ShapeLabel;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author esteban
 */
public class QueryProcessor {
    
    private static final Logger LOG = LoggerFactory.getLogger(QueryProcessor.class);
    
    private final List<Driver> drivers;

    public QueryProcessor(List<Driver> drivers) {
        this.drivers = drivers;
    }
    
    public Collection<RDFNode> query(String shEx, String shapeMap) {

        LOG.trace("ShEx expression received:\n{}", shEx);
        LOG.trace("ShapeMap expression received:\n{}", shapeMap);
        
        //Resolve the Drivers that are going to be used for this query.
        List<Driver> concreteDrivers = resolveDrivers(shEx);
        
        //Create the Model with all the data coming from the selected Drivers.
        Model model = this.createModel(shEx, concreteDrivers);
        model.setNsPrefix("dss", "http://saperi.io/dss/");
        model.setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#");
        
        //Serialize the Model as TTL
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        model.write(baos, "TURTLE");
        String modelAsString = baos.toString();
        LOG.trace("Final model:\n{}",modelAsString);
        
        //Execute de ShEx validation
        Validate validator = new Validate();
        Result result = validator.validate(modelAsString, "Turtle", shEx, "ShExC", shapeMap, "Compact").toOption().get();
        
        scala.collection.immutable.Map<RDFNode, scala.collection.immutable.Map<ShapeMapLabel, Info>> map = result.getResultShapeMap().resultMap();

        //Log results
        if (LOG.isDebugEnabled()){
            StringBuilder sb = new StringBuilder();
            map.foreach((t) -> {
                sb.append("Node: ").append(t._1.toString()).append("\n");
                t._2.foreach(m -> {
                    sb.append("\tShape: ").append(m._1.toString());
                    sb.append(" => ").append(m._2.status()).append("\n");
                    return null;
                });

                return null; //To change body of generated lambdas, choose Tools | Templates.
            });
            LOG.debug("Result:\n"+sb+"\n");
        }
        
        //Extract results
        Collection<RDFNode> resultNodes = extractResults(result);
        LOG.debug("FINAL RESULT: {}", resultNodes.stream().map(n -> n.toString()).collect(joining(",")));
        return resultNodes;
    }
    
    private List<Driver> resolveDrivers(String shEx){
        //TODO: implement a real strategy here
        return drivers.stream()
                .filter(d -> d.accept(shEx))
                .collect(toList());
    }

    private Model createModel(String shEx, List<Driver> drivers) {
        
        Model model = ModelFactory.createDefaultModel();
        
        for (Driver driver : drivers) {
            model.add(ModelFactory.createModelForGraph(driver.fetch(shEx)));
        }
        
        return model;
    }
    
    private Collection<RDFNode> extractResults(Result result){
        List<String> requestedShapes = this.toJavaList(result.getShapeMap().associations()).stream()
                .map((a) -> a.shape().toString())
                .collect(toList());
        
        Set<RDFNode> resultNodes = new HashSet<>();
        result.getResultShapeMap().resultMap().foreach((t) -> {
            RDFNode node = t._1;
            scala.collection.immutable.Map<ShapeMapLabel, Info> info = t._2;

            
            if (info.keySet().filter((k) -> {
                return (requestedShapes.contains(k.toString()) && info.get(k).get().status() == Conformant$.MODULE$ );
            }).size() > 0){
                resultNodes.add(node);
            }
            
            return null;
        });
        
        return resultNodes;
    }

    private <T> List<T> toJavaList(scala.collection.immutable.List<T> src){
        
        List<T> result = new ArrayList<>();
        
        if (src != null){
            src.foreach((v) -> {
                result.add(v);
                return null;
            });
        }
        
        return result;
        
    }
    
    private <T> Set<T> toJavaSet(scala.collection.immutable.Set<T> src){
        
        Set<T> result = new HashSet<>();
        
        if (src != null){
            src.foreach((v) -> {
                result.add(v);
                return null;
            });
        }
        
        return result;
        
    }
    
    private <K, V> Map<K, V> toJavaMap(scala.collection.immutable.Map<K, V> src){
        Map<K, V> result = new HashMap<>();
        
        if (src != null){
            src.foreach((t) -> {
                result.put(t._1, t._2);
                return null;
            });
        }
        
        return result;
    }
    
}
