/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saperi.shex.sample.poc.driver.sample;

import com.saperi.shex.sample.poc.driver.Driver;
import java.util.List;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.mem.GraphMem;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Condition;

/**
 *
 * @author esteban
 */
public class SampleConditionDriver implements Driver {

    private final DSSFhirClient fhir;

    public SampleConditionDriver(DSSFhirClient fhir) {
        this.fhir = fhir;
    }

    @Override
    public boolean accept(String shEx) {
        return true;
    }

    @Override
    public Graph fetch(String shEx) {

        Graph graph = new GraphMem();

        //TODO: what come nexts is an awful and hacky way to make progress in this POC.
        //All this MUST be re-designed and overwritten.
        String[] patientIds = SampleUtils.extractPatientIds(shEx);

        if (patientIds.length == 0) {
            //TODO (Search for all patients first and then do what we have in the else section?)
        } else {
            for (String patientId : patientIds) {

                if (shEx.contains("patient.diabetes")) {
                    List<Condition> conditions = fhir.searchResourcesForPatientAndCode(patientId, new Coding("http://snomed.info/sct", "73211009", null), Condition.class);
                    this.toTriples(graph, patientId, NodeFactory.createURI("http://saperi.io/dss/patient.diabetes"), NodeFactory.createLiteral(!conditions.isEmpty() + "", XSDDatatype.XSDboolean));
                }

            }
        }

        return graph;

    }

    private void toTriples(Graph graph, String patientId, Node extraPredicate, Node extraObject) {
        Node patientNode = NodeFactory.createURI("http://saperi.io/dss/patient/" + patientId);
        graph.add(Triple.create(patientNode, extraPredicate, extraObject));
    }

}
