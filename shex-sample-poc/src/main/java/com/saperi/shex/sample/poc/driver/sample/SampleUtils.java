/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saperi.shex.sample.poc.driver.sample;

import java.util.Arrays;
import java.util.function.Predicate;
import static java.util.stream.Collectors.toList;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;

/**
 *
 * @author esteban
 */
public class SampleUtils {
    
    public static final String XSD_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";
    
    
    public static boolean hasCoding(CodeableConcept cc, Coding coding){
        if (coding.getVersion() == null){
            return hasCoding(cc, coding.getSystem(), coding.getCode());
        }
        return hasCoding(cc, coding.getSystem(), coding.getVersion(), coding.getCode());
    }
    
    public static boolean hasCoding(CodeableConcept cc, String system, String version, String code){
        return hasCoding(
                cc, 
                c -> system.equals(c.getSystem()) && version.equals(c.getVersion()) && code.equals(c.getCode())
        );
    }
    
    public static boolean hasCoding(CodeableConcept cc, String system, String code){
        return hasCoding(
                cc, 
                c -> system.equals(c.getSystem()) && code.equals(c.getCode())
        );
    }
    
    /**
     * Horrendous (and extremely brittle) way to extract the Patient id/s from
     * a ShEx schema.
     * This method is only a workaround until a proper way to do this is defined
     * (ShEx on Shex?).
     * 
     * @param shEx
     * @return 
     */
    public static String[] extractPatientIds(String shEx){
        
        String idLine = shEx.lines().filter(l -> l.contains(":patient.id")).findFirst().orElse("");
        
        if (idLine.contains("[")){
            return Arrays.stream(
                    idLine.substring(idLine.indexOf("[")+1, idLine.indexOf("]")).split(" ")
            ).map(id -> id.replaceAll("\"", ""))
                    .collect(toList())
                    .toArray(new String[0]);
        }
        return new String[]{};
    }
    
    private static boolean hasCoding(CodeableConcept cc, Predicate<Coding> predicate ){
        if (cc == null || !cc.hasCoding()){
            return false;
        }
        
        return cc.getCoding().stream()
                .filter(predicate)
                .findAny().isPresent();
    }
}
