/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saperi.shex.sample.poc.driver.sample;

import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import com.cognitivemedicine.cdsp.fhir.client.BaseService;
import com.cognitivemedicine.cdsp.fhir.client.FhirUtil;
import com.cognitivemedicine.cdsp.fhir.client.config.FhirConfigurator;
import java.util.List;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.instance.model.api.IBaseResource;

/**
 *
 * @author esteban
 */
public class DSSFhirClient extends BaseService{

    public DSSFhirClient(IGenericClient client) {
        super(client);
    }

    public DSSFhirClient(FhirConfigurator configurator) {
        super(configurator);
    }
    
    public <T extends IBaseResource> List<T> searchResourcesForPatientAndCode(String patientId, Coding code, Class<T> clazz){
        IQuery<ca.uhn.fhir.model.api.Bundle> query = getClient().search()
                .forResource(clazz)
                .where(PARAM_PATIENT.hasId(patientId))
                .where(PARAM_CODE.exactly().systemAndCode(code.getSystem(), code.getCode()));
        
        Bundle bundle = query.returnBundle(Bundle.class).execute();
        return FhirUtil.getEntries(bundle, clazz);
    }
    
}
