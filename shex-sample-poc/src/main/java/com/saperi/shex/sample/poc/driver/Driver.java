/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.saperi.shex.sample.poc.driver;

import org.apache.jena.graph.Graph;

/**
 *
 * @author esteban
 */
public interface Driver {

    /**
     * Determines whether this Driver can or cannot uses the shEx scehma (or
     * parts of it) to fetch data from an external system.
     *
     * @param shEx the shEx schema.
     * @return whether the provided schema can be used to fetch data.
     */
    public boolean accept(String shEx);

    /**
     * Fetches all the data that can be fetched for a given ShEx and returns it
     * as an Graph.
     *
     * @param shEx the shEx expression from where the data query will be
     * derived.
     * @return The RDF Graph containing the fetched data.
     */
    public Graph fetch(String shEx);

}
