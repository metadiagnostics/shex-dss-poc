/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saperi.shex.sample.poc.driver.sample;

import com.cognitivemedicine.cdsp.fhir.client.BaseService;
import com.saperi.shex.sample.poc.driver.Driver;
import static com.saperi.shex.sample.poc.driver.sample.SampleUtils.XSD_DATE_TIME_FORMAT;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.mem.GraphMem;
import org.hl7.fhir.dstu3.model.Patient;

/**
 *
 * @author esteban
 */
public class SamplePatientDriver implements Driver {

    private final BaseService fhir;

    public SamplePatientDriver(BaseService fhir) {
        this.fhir = fhir;
    }

    @Override
    public boolean accept(String shEx) {
        return true;
    }

    @Override
    public Graph fetch(String shEx) {

        Graph graph = new GraphMem();

        //TODO: what come nexts is an awful and hacky way to make progress in this POC.
        //All this MUST be re-designed and overwritten.
        String[] patientIds = SampleUtils.extractPatientIds(shEx);

        List<Patient> patients = new ArrayList<>();
        if (patientIds.length == 0) {
            List<Patient> foundPatients = fhir.searchResourcesByType(Patient.class);
            if (foundPatients != null){
                patients.addAll(foundPatients);
            }
        } else {
            for (String patientId : patientIds) {
                Patient foundPatient = fhir.searchResourceById(patientId, Patient.class);
                if (foundPatient != null){
                    patients.add(foundPatient);
                }
            }
        }
        for (Patient patient : patients) {
            this.toTriples(graph, patient);
        }

        return graph;

    }

    private void toTriples(Graph graph, Patient patient) {

        SimpleDateFormat fmt = new SimpleDateFormat(XSD_DATE_TIME_FORMAT);

        Node patientNode = NodeFactory.createURI("http://saperi.io/dss/patient/" + patient.getIdElement().getIdPart());

        graph.add(Triple.create(patientNode, NodeFactory.createURI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), NodeFactory.createURI("http://saperi.io/dss/patient")));
        graph.add(Triple.create(patientNode, NodeFactory.createURI("http://saperi.io/dss/patient.id"), NodeFactory.createLiteral(patient.getIdElement().getIdPart())));
        graph.add(Triple.create(patientNode, NodeFactory.createURI("http://saperi.io/dss/patient.birth-date"), NodeFactory.createLiteral(fmt.format(patient.getBirthDate()), XSDDatatype.XSDdateTime)));
    }

}
