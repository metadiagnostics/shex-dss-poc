/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saperi.shex.sample.poc.driver.sample;

import com.cognitivemedicine.cdsp.fhir.client.BaseService;
import com.saperi.shex.sample.poc.driver.Driver;
import java.util.ArrayList;
import java.util.List;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.mem.GraphMem;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.MedicationAdministration;
import org.hl7.fhir.exceptions.FHIRException;

/**
 *
 * @author esteban
 */
public class SampleMedicationAdministrationDriver implements Driver {

    private final DSSFhirClient fhir;

    public SampleMedicationAdministrationDriver(DSSFhirClient fhir) {
        this.fhir = fhir;
    }

    @Override
    public boolean accept(String shEx) {
        return true;
    }

    @Override
    public Graph fetch(String shEx) {

        Graph graph = new GraphMem();

        //TODO: what come nexts is an awful and hacky way to make progress in this POC.
        //All this MUST be re-designed and overwritten.
        String[] patientIds = SampleUtils.extractPatientIds(shEx);

        if (patientIds.length == 0) {
            //TODO (Search for all patients first and then do what we have in the else section?)
        } else {
            for (String patientId : patientIds) {
                
                if (shEx.contains("patient.on-insulin")){
                    List<MedicationAdministration> mas = fhir.searchResourcesForPatientAndCode(patientId, new Coding("http://snomed.info/sct", "67866001", null), MedicationAdministration.class);
                    this.toTriples(graph, patientId, NodeFactory.createURI("http://saperi.io/dss/patient.on-insulin"), NodeFactory.createLiteral(!mas.isEmpty()+"", XSDDatatype.XSDboolean));
                }
                
                if (shEx.contains("patient.on-sulfonylurea")){
                    List<MedicationAdministration> mas = fhir.searchResourcesForPatientAndCode(patientId, new Coding("http://snomed.info/sct", "372711004", null), MedicationAdministration.class);
                    this.toTriples(graph, patientId, NodeFactory.createURI("http://saperi.io/dss/patient.on-sulfonylurea"), NodeFactory.createLiteral(!mas.isEmpty()+"", XSDDatatype.XSDboolean));
                }
                
            }
        }

        return graph;

    }

    private void toTriples(Graph graph, String patientId, Node extraPredicate, Node extraObject) {
        Node patientNode = NodeFactory.createURI("http://saperi.io/dss/patient/" + patientId);
        graph.add(Triple.create(patientNode, extraPredicate, extraObject));
    }
    
}
