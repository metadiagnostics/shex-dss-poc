
package com.saperi.shex.sample.poc;

import es.weso.rdf.jena.RDFAsJenaModel;
import es.weso.shapeMaps.FixedShapeMap;
import es.weso.shapeMaps.ResultShapeMap;
import es.weso.shapeMaps.ShapeMap;
import es.weso.shex.Schema;
import es.weso.shex.validator.Validator;
import java.util.logging.Logger;
import scala.Option;
import scala.util.Either;
import scala.util.Left;
import scala.util.Right;

public class Validate {

    Logger log = Logger.getLogger(Validate.class.getName());

    // none object is required to pass no base
    Option<String> none = Option.empty();

    public Either<String, Result> validate(String data,
        String dataFormat,
        String schema,
        String schemaFormat,
        String shapeMap,
        String shapeMapFormat) {

        RDFAsJenaModel dataModel = unwrap(readRDFFromString(data, dataFormat));

        Schema parsedSchema = unwrap(Schema.fromString(schema, schemaFormat, none, dataModel));

        ShapeMap shape = unwrap(ShapeMap.fromString(shapeMap, shapeMapFormat, none, dataModel.getPrefixMap(), parsedSchema.prefixMap()));

        FixedShapeMap fixedShapeMap = unwrap(ShapeMap.fixShapeMap(shape, dataModel, dataModel.getPrefixMap(), parsedSchema.prefixMap()));

        ResultShapeMap resultShapeMap = unwrap(Validator.validate(parsedSchema, fixedShapeMap, dataModel));

        return new Right<>(new Result(parsedSchema, dataModel, shape, resultShapeMap));
    }

    public Either<String, RDFAsJenaModel> readRDFFromString(String data, String format) {
        try {
            return RDFAsJenaModel.fromChars(data, format, none);
        } catch (Exception e) {
            return new Left<>("Error parsing data: " + e.getMessage());
        }
    }

    private <T> T unwrap(Either<String, T> either) {

        if (either.isLeft()) {
            throw new IllegalStateException(either.left().get());
        }

        return either.right().get();
    }

}
