/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.saperi.dssng.config;

import com.cognitivemedicine.cdsp.services.terminologysrv.api.CodeService;
import com.cognitivemedicine.cdsp.services.terminologysrv.impl.CodeServiceImpl;
import com.cognitivemedicine.cdsp.services.terminologysrv.model.TerminologyMappingTable;
import com.cognitivemedicine.cdsp.services.terminologysrv.model.TerminologyMappingTableFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author esteban
 */
@Configuration
public class CodeServiceConfig {

    @Bean
    public CodeService createCodeService() {
        TerminologyMappingTable mappingTable
            = TerminologyMappingTableFactory.obtainFactory(
                TerminologyMappingTableFactory.TerminologyFactoryType.CSV,
                "/terminologies/terminologies.csv")
                .createMappingTable();

        return new CodeServiceImpl(mappingTable);
    }
}
