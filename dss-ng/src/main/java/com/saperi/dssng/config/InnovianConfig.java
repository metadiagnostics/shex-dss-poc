/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.saperi.dssng.config;

import com.cognitivemedicine.cdsp.services.ds.datasource.dataaccess.innovian.CacheBasedInnovianServiceFacade;
import com.cognitivemedicine.cdsp.services.ds.datasource.dataaccess.innovian.InnovianServiceFacade;
import com.cognitivemedicine.cdsp.services.ds.datasource.handler.innovian.InnovianDataSource;
import com.cognitivemedicine.cdsp.services.innovian.api.CaseClient;
import com.cognitivemedicine.cdsp.services.innovian.api.PatientClient;
import com.cognitivemedicine.cdsp.services.innovian.impl.CaseClientImpl;
import com.cognitivemedicine.cdsp.services.innovian.impl.PatientClientImpl;
import com.cognitivemedicine.cdsp.services.innovian.impl.SercurityClientImpl;
import com.cognitivemedicine.config.utils.ConfigUtils;
import java.util.List;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author esteban
 */
@Configuration
public class InnovianConfig {

    @Value("${innovian.endpoint}")
    private String endpoint;

    @Value("${innovian.username}")
    private String username;

    @Value("${innovian.password}")
    private String password;

    public static class InnovianCookies {

        List<String> cookies;
    }

    @Bean
    public InnovianServiceFacade createInnovianServiceFacade() {

        ConfigUtils spyCU = Mockito.spy(ConfigUtils.getInstance("TMP"));
        Mockito.when(spyCU.getString(InnovianDataSource.KEY_INNOVIAN_USERNAME)).thenReturn(username);
        Mockito.when(spyCU.getString(InnovianDataSource.KEY_INNOVIAN_PASSWORD)).thenReturn(password);
        Mockito.when(spyCU.getString(ArgumentMatchers.eq(InnovianDataSource.KEY_INNOVIAN_ENDPOINT), ArgumentMatchers.anyString())).thenReturn(endpoint);

        ApplicationEventPublisher publisher = new ApplicationEventPublisher() {
            @Override
            public void publishEvent(ApplicationEvent event) {
            }

            @Override
            public void publishEvent(Object event) {
            }
        };

        return new CacheBasedInnovianServiceFacade(publisher, spyCU);
    }

    @Bean
    public InnovianCookies createAuthCookies() {
        InnovianCookies cookies = new InnovianCookies();
        cookies.cookies = new SercurityClientImpl(endpoint)
            .signOn(username, password);

        return cookies;
    }

    @Bean
    public CaseClient createCaseClient(InnovianCookies cookies) {
        CaseClient client = new CaseClientImpl(endpoint);
        client.setAuthCookies(cookies.cookies);
        return client;
    }

    @Bean
    public PatientClient createPatientClient(InnovianCookies cookies) {
        PatientClient client = new PatientClientImpl(endpoint);
        client.setAuthCookies(cookies.cookies);
        return client;
    }

}
