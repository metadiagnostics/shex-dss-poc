/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.saperi.dssng.mock;

import com.cognitivemedicine.cdsp.services.innovian.api.CaseClient;
import com.cognitivemedicine.cdsp.services.innovian.api.PatientClient;
import com.cognitivemedicine.cdsp.transformer.AbstractInnovianTransformer;
import com.draeger.medical.webservices.ArrayOfProcedureItem;
import com.draeger.medical.webservices.ArrayOfStatusItem;
import com.draeger.medical.webservices.DemographicData;
import com.draeger.medical.webservices.ExtendedMultiPartData;
import com.draeger.medical.webservices.History;
import com.draeger.medical.webservices.MultiPartDataElement;
import com.draeger.medical.webservices.NameInfo;
import com.draeger.medical.webservices.PatientInfo;
import com.draeger.medical.webservices.ProcedureCollection;
import com.draeger.medical.webservices.ProcedureItem;
import com.draeger.medical.webservices.SearchFilterTypes;
import com.draeger.medical.webservices.StatusItem;
import com.draeger.medical.webservices.StatusItemCollection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author esteban
 */
@RestController()
@RequestMapping("/mock/scenario")
public class SampleScenarioCreator {

    private static final AtomicInteger PATIENT_COUNT = new AtomicInteger(1);

    public static class Scenario {

        int patientId;
        int caseId;

        public Scenario() {
        }

        public Scenario(int patientId, int caseId) {
            this.patientId = patientId;
            this.caseId = caseId;
        }

        public int getPatientId() {
            return patientId;
        }

        public int getCaseId() {
            return caseId;
        }

    }

    @Autowired
    private CaseClient caseClient;

    @Autowired
    private PatientClient patientClient;

    @GetMapping(value = "/create", produces = {MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Scenario create() {

        PatientInfo patient = this.createNewPatient();
        int caseId = this.createNewCase(patient);
        this.createDemographics(patient, caseId);
        this.createConditions(caseId);
        this.createProcedures(caseId);

        return new Scenario(patient.getPatientId(), caseId);

    }

    private PatientInfo createNewPatient() {

        String patientMRN = PATIENT_COUNT.getAndIncrement() + "";

        NameInfo name = new NameInfo();
        name.setFirst("Name-" + patientMRN);
        name.setLast("Last-" + patientMRN);

        PatientInfo pi = new PatientInfo();
        pi.setPatientIdentificationNumber1(patientMRN);
        pi.setName(name);
        pi.setBirthDate("2000-08-06T22:33:21.000Z");
        pi.setGender(createMultiPartDataElement(0, "male"));

        int patientId = patientClient.createNewPatient(pi, true);
        pi.setPatientId(patientId);

        return pi;
    }

    private int createNewCase(PatientInfo patient) {
        return caseClient.createNewCase(
            SearchFilterTypes.MEDICAL_RECORD_NUMBER,
            patient.getPatientIdentificationNumber1(),
            null,
            null);
    }

    private void createDemographics(PatientInfo patient, int caseId) {

        DemographicData demographics = new DemographicData();
        demographics.setName(patient.getName());
        demographics.setGender(this.createMultiPartDataElement(2, "2"));
        demographics.setBirthDate(patient.getBirthDate());

        caseClient.setCase(caseId);
        caseClient.lockCaseFolder();
        caseClient.writeCaseDemographics(demographics);
        caseClient.unlockCaseFolder();
    }

    private void createConditions(int caseId) {

        StatusItem item1 = new StatusItem();
        item1.setItem(this.createMultiPartDataElement(1, "Motion Sickness"));
        item1.setUserTime(this.getDate(-1));

        StatusItem item2 = new StatusItem();
        item2.setItem(this.createMultiPartDataElement(1, "Hypertension"));
        item2.setUserTime(this.getDate(0));

        StatusItemCollection collection = new StatusItemCollection();
        collection.setItems(new ArrayOfStatusItem());
        collection.getItems().getStatusItem().add(item1);
        collection.getItems().getStatusItem().add(item2);

        History history = new History();
        history.setMedical(collection);

        caseClient.setCase(caseId);
        caseClient.lockCaseFolder();
        caseClient.writeCaseHistory(history);
        caseClient.unlockCaseFolder();
    }

    private void createProcedures(int caseId) {

        ProcedureItem item1 = new ProcedureItem();
        item1.setId(new Random().nextInt());
        item1.setName("ROPRTJ CAB/VALVE PX > 1 MO AFTER ORIGINAL OPERJ");
        item1.setLastUpdateTime(this.getDate(-3));

        ProcedureItem item2 = new ProcedureItem();
        item2.setId(new Random().nextInt());
        item2.setName("CABG W/ARTERIAL GRAFT SINGLE ARTERIAL GRAFT");
        item2.setLastUpdateTime(this.getDate(-1));

        ProcedureCollection procedures = new ProcedureCollection();
        procedures.setItems(new ArrayOfProcedureItem());
        procedures.getItems().getProcedureItem().add(item1);
        procedures.getItems().getProcedureItem().add(item2);

        caseClient.setCase(caseId);
        caseClient.lockCaseFolder();
        caseClient.writeCaseProcedures(procedures);
        caseClient.unlockCaseFolder();
    }

    private MultiPartDataElement createMultiPartDataElement(int id, String name) {
        MultiPartDataElement e = new ExtendedMultiPartData();
        e.setId(id);
        e.setName(name);

        return e;
    }

    private String getDate(int daysOffset) {
        Calendar c = new GregorianCalendar();
        c.setTime(new Date());
        c.setTimeZone(TimeZone.getTimeZone("UTC"));

        c.add(Calendar.DAY_OF_MONTH, daysOffset);

        return new SimpleDateFormat(AbstractInnovianTransformer.DEFAULT_DATE_FORMAT)
            .format(c.getTime());
    }
}
