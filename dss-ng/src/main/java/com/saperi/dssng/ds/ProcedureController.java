/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.saperi.dssng.ds;

import com.cognitivemedicine.cdsp.bom.fhir.ICORAProcedure;
import com.cognitivemedicine.cdsp.services.ds.api.scope.AllDataScope;
import com.cognitivemedicine.cdsp.services.ds.api.scope.DataScope;
import com.cognitivemedicine.cdsp.services.ds.datasource.dataaccess.innovian.InnovianServiceFacade;
import com.cognitivemedicine.cdsp.services.ds.datasource.handler.innovian.ProceduresInnovianDataSourceHandler;
import com.cognitivemedicine.cdsp.services.terminologysrv.api.CodeService;
import java.util.List;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author esteban
 */
@RestController()
@RequestMapping("/ds/procedure")
public class ProcedureController {

    @Autowired
    private InnovianServiceFacade facade;

    @Autowired
    private CodeService codeService;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON)
    public List<ICORAProcedure> list(String patientId, String caseId) {

        DataScope scope = new AllDataScope();

        ProceduresInnovianDataSourceHandler handler = new ProceduresInnovianDataSourceHandler(facade, patientId, caseId, scope, codeService);

        handler.init();
        return handler.doFetch();
    }

}
