package com.saperi.dssng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DssNgApplication {

	public static void main(String[] args) {
		SpringApplication.run(DssNgApplication.class, args);
	}
}
